import React, { Component } from "react";

export default class ExShoeCard extends Component {
  renderTable = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img src={item.image} style={{ width: "100px" }}></img>
          </td>
          <td>
            <button
              className="btn btn-success
          "
              onClick={() => {
                this.props.handleCount(item.id, -1);
              }}
            >
              -
            </button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.handleCount(item.id, +1);
              }}
            >
              +
            </button>
          </td>
          <td>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
            >
              DELETE
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table class="table">
          <thead>
            <tr>
              <th>NAME</th>
              <th>PRICE</th>
              <th>IMAGE</th>
              <th>QUANTITY</th>
              <th>ACTION</th>
            </tr>
          </thead>
          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}
