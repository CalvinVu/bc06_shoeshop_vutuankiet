import React, { Component } from "react";
import ExShoeCard from "./ExShoeCard";
import ExShoeDetail from "./ExShoeDetail";
import ExShoeList from "./ExShoeList";
import { shoeArr } from "./shoeData";
export default class ExShoe extends Component {
  state = {
    dataShoe: shoeArr,
    shoeDetail: shoeArr[0],
    cart: [],
  };
  handleChangeDetail = (item) => {
    this.setState({
      shoeDetail: item,
    });
  };
  handleAddShoe = (shoe) => {
    let cloneCart = [...this.state.cart];
    // cloneCart.push(item);
    // item.quantity = 1;
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      // let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(shoe);
      shoe.soLuong = 1;
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    });
    return cloneCart;
  };
  handleCount = (id, select) => {
    let cloneCart = [...this.state.cart];
    console.log(cloneCart);
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    console.log(index);
    cloneCart[index].soLuong = cloneCart[index].soLuong + select;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    this.setState({
      cart: cloneCart,
    });
  };
  handleDelete = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart.splice(index, 1);
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="row">
        <div className="col-4">
          <ExShoeList
            dataShoe={this.state.dataShoe}
            handleChangeDetail={this.handleChangeDetail}
            handleAddShoe={this.handleAddShoe}
          />
        </div>
        <div className="col-2">
          <ExShoeDetail shoeDetail={this.state.shoeDetail} />
        </div>
        <div className="col-6">
          <ExShoeCard
            cart={this.state.cart}
            handleCount={this.handleCount}
            handleDelete={this.handleDelete}
          />
        </div>
      </div>
    );
  }
}
