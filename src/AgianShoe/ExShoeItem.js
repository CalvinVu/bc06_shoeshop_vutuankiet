import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
      <div className="">
        <div className="card" style={{ width: "13rem" }}>
          <img
            className="card-img-top"
            src={this.props.dataListShoe.image}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{this.props.dataListShoe.name}</h5>
            <p className="card-text">{this.props.dataListShoe.price}</p>
            <button
              onClick={() => {
                console.log(this.props);
                this.props.handleChangeDetail(this.props.dataListShoe);
              }}
              className="btn btn-primary"
            >
              DETAIL
            </button>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleAddShoe(this.props.dataListShoe);
              }}
            >
              ADD
            </button>
          </div>
        </div>
      </div>
    );
  }
}
