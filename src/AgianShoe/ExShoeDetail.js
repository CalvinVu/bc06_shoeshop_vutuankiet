import React, { Component } from "react";

export default class ExShoeDetail extends Component {
  render() {
    return (
      <div>
        <div className="card" style={{ width: "13rem" }}>
          <img
            className="card-img-top"
            src={this.props.shoeDetail.image}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{this.props.shoeDetail.name}</h5>
            <p className="card-text">{this.props.shoeDetail.description}</p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
      </div>
    );
  }
}
