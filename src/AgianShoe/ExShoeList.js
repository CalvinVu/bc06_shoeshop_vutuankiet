import React, { Component } from "react";
import ExShoeItem from "./ExShoeItem";

export default class ExShoeList extends Component {
  renderListShoe = () => {
    return this.props.dataShoe.map((item, index) => {
      return (
        <ExShoeItem
          dataListShoe={item}
          key={index}
          handleChangeDetail={this.props.handleChangeDetail}
          handleAddShoe={this.props.handleAddShoe}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
